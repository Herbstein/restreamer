use mio::{net::TcpStream, Poll, PollOpt, Ready, Token};
use rml_rtmp::{
    chunk_io::Packet,
    handshake::{Handshake, HandshakeProcessResult, PeerType},
};
use std::{
    collections::VecDeque,
    fs,
    fs::File,
    io,
    io::{Read, Write},
    time::{SystemTime, UNIX_EPOCH},
};

const BUFFER_SIZE: usize = 4096;

pub enum ReadResult {
    HandshakingInProgess,
    NoBytesReceived,
    BytesReceived {
        buffer: Box<[u8; BUFFER_SIZE]>,
        byte_count: usize,
    },
    HandshakeCompleted {
        buffer: Box<[u8; BUFFER_SIZE]>,
        byte_count: usize,
    },
}

#[derive(Debug)]
pub enum ConnectionError {
    IoError(io::Error),
    SocketClosed,
}

impl From<io::Error> for ConnectionError {
    fn from(error: io::Error) -> Self {
        ConnectionError::IoError(error)
    }
}

enum SendablePacket {
    RawBytes(Vec<u8>),
    Packet(Packet),
}

struct DebugLogFiles {
    rtmp_input_file: File,
    rtmp_output_file: File,
}

pub struct Connection {
    socket: TcpStream,
    pub token: Option<Token>,
    interest: Ready,
    send_queue: VecDeque<SendablePacket>,
    has_been_registered: bool,
    handshake: Handshake,
    handshake_completed: bool,
    debug_log_files: Option<DebugLogFiles>,
    dropped_packet_count: u32,
    last_drop_notification_at: SystemTime,
}

impl Connection {
    pub fn new(
        socket: TcpStream,
        count: usize,
        log_debug_logic: bool,
        is_inbound_connection: bool,
    ) -> Connection {
        let debug_log_files = if log_debug_logic {
            fs::create_dir_all("logs").unwrap();

            let duration = SystemTime::now().duration_since(UNIX_EPOCH).unwrap();
            let seconds = duration.as_secs();
            let rtmp_input_name = format!("logs/{}-{}.rtmp.input.log", seconds, count);
            let rtmp_output_name = format!("logs/{}-{}.rtmp.output.log", seconds, count);

            let log_files = DebugLogFiles {
                rtmp_input_file: File::create(rtmp_input_name).unwrap(),
                rtmp_output_file: File::create(rtmp_output_name).unwrap(),
            };

            Some(log_files)
        } else {
            None
        };

        let handshake = if is_inbound_connection {
            Handshake::new(PeerType::Server)
        } else {
            Handshake::new(PeerType::Client)
        };

        let mut connection = Connection {
            socket,
            debug_log_files,
            token: None,
            interest: Ready::readable() | Ready::writable(),
            send_queue: VecDeque::new(),
            has_been_registered: false,
            handshake_completed: false,
            dropped_packet_count: 0,
            last_drop_notification_at: SystemTime::now(),
            handshake,
        };

        let handshake_bytes = connection.handshake.generate_outbound_p0_and_p1().unwrap();
        connection
            .send_queue
            .push_back(SendablePacket::RawBytes(handshake_bytes));
        connection.interest.insert(Ready::writable());
        connection
    }

    pub fn enqueue_response(&mut self, poll: &mut Poll, bytes: Vec<u8>) -> io::Result<()> {
        self.send_queue.push_back(SendablePacket::RawBytes(bytes));
        self.interest.insert(Ready::writable());
        self.register(poll)
    }

    pub fn enqueue_packet(&mut self, poll: &mut Poll, packet: Packet) -> io::Result<()> {
        let elapsed = self.last_drop_notification_at.elapsed().unwrap();
        if elapsed.as_secs() > 10 {
            if self.dropped_packet_count > 0 {
                println!(
                    "{} packets dropped in the last {} seconds",
                    self.dropped_packet_count,
                    elapsed.as_secs()
                );
            }

            self.last_drop_notification_at = SystemTime::now();
            self.dropped_packet_count = 0;
        }

        if packet.can_be_dropped && self.send_queue.len() > 10 {
            self.dropped_packet_count += 1;
            Ok(())
        } else {
            self.send_queue.push_back(SendablePacket::Packet(packet));
            self.interest.insert(Ready::writable());
            self.register(poll)
        }
    }

    pub fn readable(&mut self, poll: &mut Poll) -> Result<ReadResult, ConnectionError> {
        let mut buffer = [0; 4096];
        match self.socket.read(&mut buffer) {
            Ok(0) => Err(ConnectionError::SocketClosed),

            Ok(bytes_read_count) => {
                let read_result = if self.handshake_completed {
                    ReadResult::BytesReceived {
                        buffer: Box::new(buffer),
                        byte_count: bytes_read_count,
                    }
                } else {
                    self.handle_handshake_bytes(poll, &buffer[..bytes_read_count])?
                };

                if let ReadResult::BytesReceived {
                    buffer: ref read_buffer,
                    ..
                } = read_result
                {
                    match self.debug_log_files {
                        None => (),
                        Some(ref mut logs) => {
                            logs.rtmp_input_file.write_all(&**read_buffer).unwrap();
                        }
                    }
                }

                self.register(poll)?;
                Ok(read_result)
            }

            Err(error) => match error.kind() {
                io::ErrorKind::WouldBlock => {
                    self.register(poll)?;
                    Ok(ReadResult::NoBytesReceived)
                }
                _ => {
                    println!(
                        "Failed to send buffer for {:?} with error {}",
                        self.token, error
                    );
                    Err(ConnectionError::IoError(error))
                }
            },
        }
    }

    pub fn writable(&mut self, poll: &mut Poll) -> io::Result<()> {
        let message = match self.send_queue.pop_front() {
            Some(x) => x,
            None => {
                self.interest.remove(Ready::writable());
                self.register(poll)?;
                return Ok(());
            }
        };

        let bytes = match message {
            SendablePacket::RawBytes(bytes) => bytes,
            SendablePacket::Packet(packet) => packet.bytes,
        };

        match self.socket.write(&bytes) {
            Ok(_) => if self.handshake_completed {
                match self.debug_log_files {
                    None => (),
                    Some(ref mut logs) => {
                        logs.rtmp_output_file.write_all(&bytes).unwrap();
                    }
                }
            },

            Err(error) => match error.kind() {
                io::ErrorKind::WouldBlock => {
                    println!("Full write buffer!");
                    self.send_queue.push_front(SendablePacket::RawBytes(bytes));
                }
                _ => {
                    println!(
                        "Failed to send buffer for {:?} with error {}",
                        self.token, error
                    );
                    return Err(error);
                }
            },
        };

        if self.send_queue.is_empty() {
            self.interest.remove(Ready::writable())
        }

        self.register(poll)?;
        Ok(())
    }

    pub fn register(&mut self, poll: &mut Poll) -> io::Result<()> {
        if self.has_been_registered {
            poll.reregister(
                &self.socket,
                self.token.unwrap(),
                self.interest,
                PollOpt::edge() | PollOpt::oneshot(),
            )?
        } else {
            poll.register(
                &self.socket,
                self.token.unwrap(),
                self.interest,
                PollOpt::edge() | PollOpt::oneshot(),
            )?
        }

        self.has_been_registered = true;
        Ok(())
    }

    fn handle_handshake_bytes(
        &mut self,
        poll: &mut Poll,
        bytes: &[u8],
    ) -> Result<ReadResult, ConnectionError> {
        let result = match self.handshake.process_bytes(bytes) {
            Ok(result) => result,
            Err(error) => {
                println!("Handshake error: {:?}", error);
                return Err(ConnectionError::SocketClosed);
            }
        };

        match result {
            HandshakeProcessResult::InProgress { response_bytes } => {
                if !response_bytes.is_empty() {
                    self.enqueue_response(poll, response_bytes)?;
                }

                Ok(ReadResult::HandshakingInProgess)
            }

            HandshakeProcessResult::Completed {
                response_bytes,
                remaining_bytes,
            } => {
                println!("Handshake successful!");
                if !response_bytes.is_empty() {
                    self.enqueue_response(poll, response_bytes)?;
                }

                let mut buffer = [0; BUFFER_SIZE];
                let buffer_size = remaining_bytes.len();
                for (index, value) in remaining_bytes.into_iter().enumerate() {
                    buffer[index] = value;
                }

                self.handshake_completed = true;

                Ok(ReadResult::HandshakeCompleted {
                    buffer: Box::new(buffer),
                    byte_count: buffer_size,
                })
            }
        }
    }
}
