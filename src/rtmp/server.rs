use bytes::Bytes;
use libc;
use rml_rtmp::{
    chunk_io::Packet,
    sessions::{
        ClientSession, ClientSessionConfig, ClientSessionEvent, ClientSessionResult,
        PublishRequestType, ServerSession, ServerSessionConfig, ServerSessionEvent,
        ServerSessionResult, StreamMetadata,
    },
    time::RtmpTimestamp,
};
use shell::ShellChild;
use slab::Slab;
use std::{
    collections::{hash_map::Entry::*, HashMap, HashSet},
    fs,
    path::Path,
    process::Stdio,
    rc::Rc,
};
use PushOptions;

enum ReceivedDataType {
    Audio,
    Video,
}

enum InboundClientAction {
    Waiting,
    Publishing(String),
    Watching { stream_key: String, stream_id: u32 },
}

struct InboundClient {
    session: ServerSession,
    current_action: InboundClientAction,
    connection_id: usize,
    has_received_video_keyframe: bool,
}

impl InboundClient {
    fn get_active_stream_id(&self) -> Option<u32> {
        match self.current_action {
            InboundClientAction::Waiting | InboundClientAction::Publishing(_) => None,
            InboundClientAction::Watching { stream_id, .. } => Some(stream_id),
        }
    }
}

enum PullState {
    Handshaking,
    Connecting,
    Connected,
    Pulling,
}

struct PullClient {
    session: ClientSession,
    connection_id: usize,
    pull_app: String,
    pull_stream: String,
    pull_target_stream: String,
    state: PullState,
}

#[derive(PartialEq, Clone)]
enum PushState {
    Inactive,
    WaitingForConnection,
    Handshaking,
    Connecting,
    Connected,
    Pushing,
}

struct PushClient {
    session: ClientSession,
    connection_id: Option<usize>,
    push_app: String,
    push_source_stream: String,
    push_target_stream: String,
    state: PushState,
}

struct MediaChannel {
    publishing_client_id: Option<usize>,
    watching_client_ids: HashSet<usize>,
    metadata: Option<Rc<StreamMetadata>>,
    video_sequence_header: Option<Bytes>,
    audio_sequence_header: Option<Bytes>,
    ffmpeg_child: Option<ShellChild>,
}

#[derive(Debug)]
pub enum ServerResult {
    DisconnectConnection {
        connection_id: usize,
    },
    OutboundPacket {
        target_connection_id: usize,
        packet: Packet,
    },
    StartPushing,
}

pub struct Server {
    clients: Slab<InboundClient>,
    connection_to_client_map: HashMap<usize, usize>,
    channels: HashMap<String, MediaChannel>,
    pull_client: Option<PullClient>,
    push_client: Option<PushClient>,
}

impl Server {
    pub fn new(push_options: &Option<PushOptions>) -> Server {
        let push_client = match *push_options {
            None => None,
            Some(ref options) => Some(PushClient {
                push_app: options.app.clone(),
                push_source_stream: options.source_stream.clone(),
                push_target_stream: options.target_stream.clone(),
                connection_id: None,
                session: ClientSession::new(ClientSessionConfig::new()),
                state: PushState::Inactive,
            }),
        };

        Server {
            clients: Slab::with_capacity(1024),
            connection_to_client_map: HashMap::with_capacity(1024),
            channels: HashMap::new(),
            pull_client: None,
            push_client,
        }
    }

    pub fn register_pull_client(
        &mut self,
        connection_id: usize,
        app: String,
        stream: String,
        target_stream: String,
    ) {
        let config = ClientSessionConfig::new();

        self.channels
            .entry(target_stream.clone())
            .or_insert(MediaChannel {
                publishing_client_id: Some(connection_id),
                watching_client_ids: HashSet::new(),
                metadata: None,
                video_sequence_header: None,
                audio_sequence_header: None,
                ffmpeg_child: None,
            });

        self.pull_client = Some(PullClient {
            session: ClientSession::new(config),
            pull_app: app,
            pull_stream: stream,
            pull_target_stream: target_stream,
            state: PullState::Handshaking,
            connection_id,
        });
    }

    pub fn register_push_client(&mut self, connection_id: usize) {
        if let Some(ref mut client) = self.push_client {
            client.connection_id = Some(connection_id);
            client.state = PushState::Handshaking;
        }
    }

    pub fn bytes_received(
        &mut self,
        connection_id: usize,
        bytes: &[u8],
    ) -> Result<Vec<ServerResult>, String> {
        let mut server_results = Vec::new();

        let push_client_connection_id = self.push_client.as_ref().and_then(|c| {
            if let Some(connection_id) = c.connection_id {
                Some(connection_id)
            } else {
                None
            }
        });

        let pull_client_connection_id = self.pull_client.as_ref().and_then(|_| Some(connection_id));

        if pull_client_connection_id
            .as_ref()
            .map_or(false, |id| *id == connection_id)
        {
            let session_result = match self
                .pull_client
                .as_mut()
                .unwrap()
                .session
                .handle_input(bytes)
            {
                Ok(results) => results,
                Err(error) => return Err(error.to_string()),
            };

            self.handle_pull_session_results(session_result, &mut server_results);
        } else if push_client_connection_id
            .as_ref()
            .map_or(false, |id| *id == connection_id)
        {
            let session_results = if let Some(ref mut push_client) = self.push_client {
                match push_client.session.handle_input(bytes) {
                    Ok(results) => results,
                    Err(error) => return Err(error.to_string()),
                }
            } else {
                Vec::new()
            };

            self.handle_push_session_results(session_results, &mut server_results);
        } else {
            if let Vacant(_) = self.connection_to_client_map.entry(connection_id) {
                let config = ServerSessionConfig::new();
                let (session, initial_session_results) = match ServerSession::new(config) {
                    Ok(results) => results,
                    Err(error) => return Err(error.to_string()),
                };

                self.handle_server_session_results(
                    connection_id,
                    initial_session_results,
                    &mut server_results,
                );

                let client = InboundClient {
                    session,
                    connection_id,
                    current_action: InboundClientAction::Waiting,
                    has_received_video_keyframe: false,
                };

                let client_id = Some(self.clients.insert(client));
                self.connection_to_client_map
                    .insert(connection_id, client_id.unwrap());
            }

            let client_results;
            {
                let client_id = &self.connection_to_client_map[&connection_id];
                let client = self.clients.get_mut(*client_id).unwrap();
                client_results = match client.session.handle_input(bytes) {
                    Ok(results) => results,
                    Err(error) => return Err(error.to_string()),
                };
            }

            self.handle_server_session_results(connection_id, client_results, &mut server_results);
        }

        Ok(server_results)
    }

    pub fn notify_connection_closed(&mut self, connection_id: usize) {
        if self
            .pull_client
            .as_ref()
            .map_or(false, |c| c.connection_id == connection_id)
        {
            self.pull_client = None
        } else {
            match self.connection_to_client_map.remove(&connection_id) {
                None => (),
                Some(client_id) => {
                    let client = self.clients.remove(client_id);
                    match client.current_action {
                        InboundClientAction::Publishing(stream_key) => {
                            self.publishing_ended(&stream_key)
                        }
                        InboundClientAction::Watching { stream_key, .. } => {
                            self.play_ended(client_id, &stream_key)
                        }
                        InboundClientAction::Waiting => (),
                    }
                }
            }
        }
    }

    fn handle_server_session_results(
        &mut self,
        executed_connection_id: usize,
        session_results: Vec<ServerSessionResult>,
        server_results: &mut Vec<ServerResult>,
    ) {
        for result in session_results {
            match result {
                ServerSessionResult::OutboundResponse(packet) => {
                    server_results.push(ServerResult::OutboundPacket {
                        target_connection_id: executed_connection_id,
                        packet,
                    })
                }

                ServerSessionResult::RaisedEvent(event) => {
                    self.handle_raised_event(executed_connection_id, event, server_results)
                }

                x => println!("Server result received: {:?}", x),
            }
        }
    }

    fn handle_raised_event(
        &mut self,
        executed_connection_id: usize,
        event: ServerSessionEvent,
        server_results: &mut Vec<ServerResult>,
    ) {
        match event {
            ServerSessionEvent::ConnectionRequested {
                request_id,
                app_name,
            } => {
                self.handle_connection_requested(
                    executed_connection_id,
                    request_id,
                    &app_name,
                    server_results,
                );
            }

            ServerSessionEvent::PublishStreamRequested {
                request_id,
                app_name,
                stream_key,
                ..
            } => {
                self.handle_publish_requested(
                    executed_connection_id,
                    request_id,
                    &app_name,
                    &stream_key,
                    server_results,
                );
            }

            ServerSessionEvent::PlayStreamRequested {
                request_id,
                app_name,
                stream_key,
                stream_id,
                ..
            } => {
                self.handle_play_requested(
                    executed_connection_id,
                    request_id,
                    &app_name,
                    &stream_key,
                    stream_id,
                    server_results,
                );
            }

            ServerSessionEvent::StreamMetadataChanged {
                app_name,
                stream_key,
                metadata,
            } => {
                self.handle_metadata_received(&app_name, &stream_key, metadata, server_results);
            }

            ServerSessionEvent::VideoDataReceived {
                stream_key,
                data,
                timestamp,
                ..
            } => {
                self.handle_audio_video_data_received(
                    &stream_key,
                    timestamp,
                    &data,
                    &ReceivedDataType::Video,
                    server_results,
                );
            }

            ServerSessionEvent::AudioDataReceived {
                stream_key,
                data,
                timestamp,
                ..
            } => {
                self.handle_audio_video_data_received(
                    &stream_key,
                    timestamp,
                    &data,
                    &ReceivedDataType::Audio,
                    server_results,
                );
            }

            _ => println!(
                "Event raised by connection {}: {:?}",
                executed_connection_id, event
            ),
        }
    }

    fn handle_connection_requested(
        &mut self,
        requested_connection_id: usize,
        request_id: u32,
        app_name: &str,
        server_results: &mut Vec<ServerResult>,
    ) {
        println!(
            "Connection {} requested connection to app '{}'",
            requested_connection_id, app_name
        );

        let accept_result;
        {
            let client_id = &self.connection_to_client_map[&requested_connection_id];
            let client = self.clients.get_mut(*client_id).unwrap();
            accept_result = client.session.accept_request(request_id);
        }

        match accept_result {
            Err(error) => {
                println!("Error occurred accepting connection request: {:?}", error);
                server_results.push(ServerResult::DisconnectConnection {
                    connection_id: requested_connection_id,
                })
            }

            Ok(results) => {
                self.handle_server_session_results(
                    requested_connection_id,
                    results,
                    server_results,
                );
            }
        }
    }

    fn handle_publish_requested(
        &mut self,
        requested_connection_id: usize,
        request_id: u32,
        app_name: &str,
        stream_key: &str,
        server_results: &mut Vec<ServerResult>,
    ) {
        println!(
            "Publish requested on app '{}' and stream key '{}'",
            app_name, stream_key
        );

        match self.channels.get(stream_key) {
            None => (),
            Some(channel) => match channel.publishing_client_id {
                None => (),
                Some(_) => {
                    println!("Stream key already being published to");
                    server_results.push(ServerResult::DisconnectConnection {
                        connection_id: requested_connection_id,
                    });
                    return;
                }
            },
        }

        let accept_result;
        {
            let client_id = &self.connection_to_client_map[&requested_connection_id];
            let client = self.clients.get_mut(*client_id).unwrap();
            client.current_action = InboundClientAction::Publishing(stream_key.to_string());

            let channel = self
                .channels
                .entry(stream_key.to_string())
                .or_insert(MediaChannel {
                    publishing_client_id: None,
                    watching_client_ids: HashSet::new(),
                    metadata: None,
                    video_sequence_header: None,
                    audio_sequence_header: None,
                    ffmpeg_child: None,
                });

            channel.publishing_client_id = Some(*client_id);

            let playlist_path = Path::new("public/videos").join(stream_key);

            if fs::create_dir_all(playlist_path.clone()).is_ok() {
                ()
            }

            let ffmpeg_command = "ffmpeg -i rtmp://localhost:1935/live/{} -b:v 7400k -maxrate 8000k -bufsize 1000k -c:v h264 -c:a aac -ar 48000 -b:a 192k -ac 1 -strict -2 -crf 20 -profile:v main -g 48 -keyint_min 48 -sc_threshold 0 -pix_fmt yuv420p -flags -global_header -hls_time 0 -hls_list_size 2 -hls_wrap 10 -start_number 0 public/videos/{}/output.m3u8";

            let mut cmd = cmd!(ffmpeg_command, stream_key, stream_key);
            {
                let command = &mut cmd.command;
                command.stdin(Stdio::null());
                command.stderr(Stdio::null());
                command.stdin(Stdio::null());
            }

            match cmd.spawn() {
                Ok(child) => channel.ffmpeg_child = Some(child),
                Err(error) => println!("{:?}", error),
            }

            accept_result = client.session.accept_request(request_id);
        }

        match accept_result {
            Err(error) => {
                println!("Error occurred accepting publish request: {:?}", error);
                server_results.push(ServerResult::DisconnectConnection {
                    connection_id: requested_connection_id,
                })
            }

            Ok(results) => {
                if let Some(ref mut client) = self.push_client {
                    if client.state == PushState::Inactive {
                        if app_name == client.push_app && stream_key == client.push_source_stream {
                            println!("Publishing on the push source stream key!");
                            client.state = PushState::WaitingForConnection;
                            server_results.push(ServerResult::StartPushing)
                        } else {
                            println!("Not publishing on the push source stream key!");
                        }
                    }
                }

                self.handle_server_session_results(
                    requested_connection_id,
                    results,
                    server_results,
                );
            }
        }
    }

    fn handle_play_requested(
        &mut self,
        requested_connection_id: usize,
        request_id: u32,
        app_name: &str,
        stream_key: &str,
        stream_id: u32,
        server_results: &mut Vec<ServerResult>,
    ) {
        println!(
            "Play requested on app '{}' and stream key '{}'",
            app_name, stream_key
        );

        let accept_result;
        {
            let client_id = &self.connection_to_client_map[&requested_connection_id];
            let client = self.clients.get_mut(*client_id).unwrap();
            client.current_action = InboundClientAction::Watching {
                stream_key: stream_key.to_string(),
                stream_id,
            };

            let channel = self
                .channels
                .entry(stream_key.to_string())
                .or_insert(MediaChannel {
                    publishing_client_id: None,
                    watching_client_ids: HashSet::new(),
                    metadata: None,
                    video_sequence_header: None,
                    audio_sequence_header: None,
                    ffmpeg_child: None,
                });

            channel.watching_client_ids.insert(*client_id);
            accept_result = match client.session.accept_request(request_id) {
                Err(error) => Err(error),
                Ok(mut results) => {
                    // If the channel already has existing metadata, send that to the new client
                    // so they have up to date info
                    match channel.metadata {
                        None => (),
                        Some(ref metadata) => {
                            let packet = match client
                                .session
                                .send_metadata(stream_id, metadata.clone())
                            {
                                Ok(packet) => packet,
                                Err(error) => {
                                    println!("Error occurred sending existing metadata to new client: {:?}", error);
                                    server_results.push(ServerResult::DisconnectConnection {
                                        connection_id: requested_connection_id,
                                    });

                                    return;
                                }
                            };

                            results.push(ServerSessionResult::OutboundResponse(packet));
                        }
                    }

                    // If the channel already has sequence headers, send them
                    match channel.video_sequence_header {
                        None => (),
                        Some(ref data) => {
                            let packet = match client.session.send_video_data(
                                stream_id,
                                data.clone(),
                                RtmpTimestamp::new(0),
                                false,
                            ) {
                                Ok(packet) => packet,
                                Err(error) => {
                                    println!(
                                        "Error occurred sending video header to new client: {:?}",
                                        error
                                    );
                                    server_results.push(ServerResult::DisconnectConnection {
                                        connection_id: requested_connection_id,
                                    });

                                    return;
                                }
                            };

                            results.push(ServerSessionResult::OutboundResponse(packet));
                        }
                    }

                    match channel.audio_sequence_header {
                        None => (),
                        Some(ref data) => {
                            let packet = match client.session.send_audio_data(
                                stream_id,
                                data.clone(),
                                RtmpTimestamp::new(0),
                                false,
                            ) {
                                Ok(packet) => packet,
                                Err(error) => {
                                    println!(
                                        "Error occurred sending audio header to new client: {:?}",
                                        error
                                    );
                                    server_results.push(ServerResult::DisconnectConnection {
                                        connection_id: requested_connection_id,
                                    });

                                    return;
                                }
                            };

                            results.push(ServerSessionResult::OutboundResponse(packet));
                        }
                    }

                    Ok(results)
                }
            }
        }

        match accept_result {
            Err(error) => {
                println!("Error occurred accepting playback request: {:?}", error);
                server_results.push(ServerResult::DisconnectConnection {
                    connection_id: requested_connection_id,
                });

                return;
            }

            Ok(results) => {
                self.handle_server_session_results(
                    requested_connection_id,
                    results,
                    server_results,
                );
            }
        }
    }

    fn handle_metadata_received(
        &mut self,
        app_name: &str,
        stream_key: &str,
        metadata: StreamMetadata,
        server_results: &mut Vec<ServerResult>,
    ) {
        println!(
            "New metadata received for app '{}' and stream key '{}'",
            app_name, stream_key
        );

        let channel = match self.channels.get_mut(stream_key) {
            Some(channel) => channel,
            None => return,
        };

        let metadata = Rc::new(metadata);
        channel.metadata = Some(metadata.clone());

        // Send the metadata to all current watchers
        for client_id in &channel.watching_client_ids {
            let client = match self.clients.get_mut(*client_id) {
                Some(client) => client,
                None => continue,
            };

            let active_stream_id = match client.get_active_stream_id() {
                Some(stream_id) => stream_id,
                None => continue,
            };

            match client
                .session
                .send_metadata(active_stream_id, metadata.clone())
            {
                Ok(packet) => server_results.push(ServerResult::OutboundPacket {
                    target_connection_id: client.connection_id,
                    packet,
                }),

                Err(error) => {
                    println!(
                        "Error sending metadata to client on connection id {}: {:?}",
                        client.connection_id, error
                    );
                    server_results.push(ServerResult::DisconnectConnection {
                        connection_id: client.connection_id,
                    });
                }
            }
        }
    }

    fn handle_audio_video_data_received(
        &mut self,
        stream_key: &str,
        timestamp: RtmpTimestamp,
        data: &Bytes,
        data_type: &ReceivedDataType,
        server_results: &mut Vec<ServerResult>,
    ) {
        {
            let channel = match self.channels.get_mut(stream_key) {
                Some(channel) => channel,
                None => return,
            };

            // If this is an audio or video sequence header we need to save it, so it can be
            // distributed to any late coming watchers
            match data_type {
                ReceivedDataType::Video => {
                    if is_video_sequence_header(&data) {
                        channel.video_sequence_header = Some(data.clone());
                    }
                }

                ReceivedDataType::Audio => {
                    if is_audio_sequence_header(&data) {
                        channel.audio_sequence_header = Some(data.clone());
                    }
                }
            }

            for client_id in &channel.watching_client_ids {
                let client = match self.clients.get_mut(*client_id) {
                    Some(client) => client,
                    None => continue,
                };

                let active_stream_id = match client.get_active_stream_id() {
                    Some(stream_id) => stream_id,
                    None => continue,
                };

                let should_send_to_client = match data_type {
                    ReceivedDataType::Video => {
                        client.has_received_video_keyframe
                            || (is_video_sequence_header(&data) || is_video_keyframe(&data.clone()))
                    }

                    ReceivedDataType::Audio => {
                        client.has_received_video_keyframe || is_audio_sequence_header(&data)
                    }
                };

                if !should_send_to_client {
                    continue;
                }

                let send_result = match data_type {
                    ReceivedDataType::Audio => client.session.send_audio_data(
                        active_stream_id,
                        data.clone(),
                        timestamp,
                        true,
                    ),
                    ReceivedDataType::Video => {
                        if is_video_keyframe(&data.clone()) {
                            client.has_received_video_keyframe = true;
                        }

                        client.session.send_video_data(
                            active_stream_id,
                            data.clone(),
                            timestamp,
                            true,
                        )
                    }
                };

                match send_result {
                    Ok(packet) => server_results.push(ServerResult::OutboundPacket {
                        target_connection_id: client.connection_id,
                        packet,
                    }),

                    Err(error) => {
                        println!(
                            "Error sending a/v data to client on connection id {}: {:?}",
                            client.connection_id, error
                        );
                        server_results.push(ServerResult::DisconnectConnection {
                            connection_id: client.connection_id,
                        });
                    }
                }
            }
        }

        let mut push_results = Vec::new();
        {
            if let Some(ref mut client) = self.push_client {
                if client.state == PushState::Pushing {
                    let result = match data_type {
                        ReceivedDataType::Video => {
                            client
                                .session
                                .publish_video_data(data.clone(), timestamp, true)
                        }

                        ReceivedDataType::Audio => {
                            client
                                .session
                                .publish_audio_data(data.clone(), timestamp, true)
                        }
                    };

                    match result {
                        Ok(client_result) => push_results.push(client_result),
                        Err(error) => {
                            println!("Error sending a/v data to push client: {:?}", error);
                        }
                    }
                }
            }
        }

        if !push_results.is_empty() {
            self.handle_push_session_results(push_results, server_results);
        }
    }

    fn publishing_ended(&mut self, stream_key: &str) {
        let channel = match self.channels.get_mut(stream_key) {
            Some(channel) => channel,
            None => return,
        };

        if let Some(ref mut ffmpeg_child) = channel.ffmpeg_child {
            ffmpeg_child
                .signal(libc::SIGKILL)
                .expect("Failed sending SIGINT?");
        }

        channel.ffmpeg_child = None;
        channel.publishing_client_id = None;
        channel.metadata = None;
    }

    fn play_ended(&mut self, client_id: usize, stream_key: &str) {
        let channel = match self.channels.get_mut(stream_key) {
            Some(channel) => channel,
            None => return,
        };

        channel.watching_client_ids.remove(&client_id);
    }

    fn handle_pull_session_results(
        &mut self,
        session_results: Vec<ClientSessionResult>,
        server_results: &mut Vec<ServerResult>,
    ) {
        let mut new_results = Vec::new();
        let mut events = Vec::new();

        if let Some(ref mut client) = self.pull_client {
            for result in session_results {
                match result {
                    ClientSessionResult::OutboundResponse(packet) => {
                        server_results.push(ServerResult::OutboundPacket {
                            target_connection_id: client.connection_id,
                            packet,
                        });
                    }
                    ClientSessionResult::RaisedEvent(event) => {
                        events.push(event);
                    }

                    x => println!("Client result received: {:?}", x),
                }
            }

            if let PullState::Handshaking = client.state {
                client.state = PullState::Connecting;
                let result = client
                    .session
                    .request_connection(client.pull_app.clone())
                    .unwrap();
                new_results.push(result);
            }
        }

        if !new_results.is_empty() {
            self.handle_pull_session_results(new_results, server_results);
        }

        for event in events {
            match event {
                ClientSessionEvent::ConnectionRequestAccepted => {
                    self.handle_pull_connection_accepted_event(server_results);
                }

                ClientSessionEvent::PlaybackRequestAccepted => {
                    self.handle_pull_playback_accepted_event(server_results);
                }

                ClientSessionEvent::VideoDataReceived { data, timestamp } => {
                    self.handle_pull_audio_video_data_received(
                        &data,
                        &ReceivedDataType::Video,
                        timestamp,
                        server_results,
                    );
                }

                ClientSessionEvent::AudioDataReceived { data, timestamp } => {
                    self.handle_pull_audio_video_data_received(
                        &data,
                        &ReceivedDataType::Audio,
                        timestamp,
                        server_results,
                    );
                }

                ClientSessionEvent::StreamMetadataReceived { metadata } => {
                    self.handle_pull_metadata_received(metadata, server_results);
                }

                x => println!("Unhandled event raised: {:?}", x),
            }
        }
    }

    fn handle_pull_connection_accepted_event(&mut self, server_results: &mut Vec<ServerResult>) {
        let mut new_results = Vec::new();
        if let Some(ref mut client) = self.pull_client {
            println!("Pull accepted for app '{}'", client.pull_app);
            client.state = PullState::Connected;

            let result = client
                .session
                .request_playback(client.pull_stream.clone())
                .unwrap();

            let mut results = vec![result];
            new_results.append(&mut results);
        }

        if !new_results.is_empty() {
            self.handle_pull_session_results(new_results, server_results);
        }
    }

    fn handle_pull_playback_accepted_event(&mut self, _server_results: &mut Vec<ServerResult>) {
        if let Some(ref mut client) = self.pull_client {
            println!("Playback accepted for stream '{}'", client.pull_stream);
            client.state = PullState::Pulling;
        }
    }

    fn handle_pull_audio_video_data_received(
        &mut self,
        data: &Bytes,
        data_type: &ReceivedDataType,
        timestamp: RtmpTimestamp,
        server_results: &mut Vec<ServerResult>,
    ) {
        let stream_key = match self.pull_client {
            Some(ref client) => client.pull_target_stream.clone(),
            None => return,
        };

        self.handle_audio_video_data_received(
            stream_key.as_str(),
            timestamp,
            &data,
            &data_type,
            server_results,
        );
    }

    fn handle_pull_metadata_received(
        &mut self,
        metadata: StreamMetadata,
        server_results: &mut Vec<ServerResult>,
    ) {
        let (app_name, stream_key) = match self.pull_client {
            Some(ref client) => (client.pull_target_stream.clone(), client.pull_app.clone()),
            None => return,
        };

        self.handle_metadata_received(&app_name, &stream_key, metadata, server_results);
    }

    fn handle_push_session_results(
        &mut self,
        session_results: Vec<ClientSessionResult>,
        server_results: &mut Vec<ServerResult>,
    ) {
        let mut new_results = Vec::new();
        let mut events = Vec::new();

        if let Some(ref mut client) = self.push_client {
            for result in session_results {
                match result {
                    ClientSessionResult::OutboundResponse(packet) => {
                        server_results.push(ServerResult::OutboundPacket {
                            target_connection_id: client.connection_id.unwrap(),
                            packet,
                        });
                    }

                    ClientSessionResult::RaisedEvent(event) => {
                        events.push(event);
                    }

                    x => println!("Push client result received: {:?}", x),
                }
            }

            if let PushState::Handshaking = client.state {
                client.state = PushState::Connecting;
                let result = match client.session.request_connection(client.push_app.clone()) {
                    Ok(result) => result,
                    Err(error) => {
                        println!("Failed to request connection for push client: {:?}", error);
                        return;
                    }
                };

                new_results.push(result);
            }
        }

        if !new_results.is_empty() {
            self.handle_push_session_results(new_results, server_results);
        }

        for event in events {
            match event {
                ClientSessionEvent::ConnectionRequestAccepted => {
                    self.handle_push_connection_accepted_event(server_results)
                }
                ClientSessionEvent::PublishRequestAccepted => {
                    self.handle_push_publish_accepted_event(server_results)
                }
                x => println!("Push event raised: {:?}", x),
            }
        }
    }

    fn handle_push_connection_accepted_event(&mut self, server_results: &mut Vec<ServerResult>) {
        let mut new_results = Vec::new();

        if let Some(ref mut client) = self.push_client {
            println!("push accepted for app '{}'", client.push_app);
            client.state = PushState::Connected;

            let result = client
                .session
                .request_publishing(client.push_target_stream.clone(), PublishRequestType::Live)
                .unwrap();

            let mut results = vec![result];
            new_results.append(&mut results);
        }

        if !new_results.is_empty() {
            self.handle_push_session_results(new_results, server_results);
        }
    }

    fn handle_push_publish_accepted_event(&mut self, server_results: &mut Vec<ServerResult>) {
        let mut new_results = Vec::new();

        if let Some(ref mut client) = self.push_client {
            println!(
                "Publish accepted for push stream key {}",
                client.push_target_stream
            );

            client.state = PushState::Pushing;

            if let Some(ref channel) = self.channels.get(&client.push_source_stream) {
                if let Some(ref metadata) = channel.metadata {
                    let result = client.session.publish_metadata(&metadata).unwrap();
                    new_results.push(result);
                }

                if let Some(ref bytes) = channel.video_sequence_header {
                    let result = client
                        .session
                        .publish_video_data(bytes.clone(), RtmpTimestamp::new(0), false)
                        .unwrap();

                    new_results.push(result);
                }

                if let Some(ref bytes) = channel.audio_sequence_header {
                    let result = client
                        .session
                        .publish_audio_data(bytes.clone(), RtmpTimestamp::new(0), false)
                        .unwrap();

                    new_results.push(result);
                }
            }
        }

        if !new_results.is_empty() {
            self.handle_push_session_results(new_results, server_results);
        }
    }
}

fn is_video_sequence_header(data: &Bytes) -> bool {
    // This is assuming h264.
    data.len() >= 2 && data[0] == 0x17 && data[1] == 0x00
}

fn is_audio_sequence_header(data: &Bytes) -> bool {
    // This is assuming aac
    data.len() >= 2 && data[0] == 0xaf && data[1] == 0x00
}

fn is_video_keyframe(data: &Bytes) -> bool {
    // assumings h264
    data.len() >= 2 && data[0] == 0x17 && data[1] != 0x00 // 0x00 is the sequence header, don't count that for now
}
