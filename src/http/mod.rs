use actix_web;
use actix_web::{
    fs::NamedFile,
    http::{Method, StatusCode},
    server, App, HttpRequest, HttpResponse, Responder, Result, State,
};
use std::path::{Path, PathBuf};
use tera;

struct AppState {
    template: tera::Tera,
}

pub fn http_server() {
    server::new(|| {
        App::with_state(AppState {
            template: compile_templates!(concat!(env!("CARGO_MANIFEST_DIR"), "/templates/**/*")),
        }).resource("/{streamkey}", |r| r.method(Method::GET).with(stream_page))
        .resource("/public/{tail:.*}", |r| {
            r.method(Method::GET).f(public_static)
        }).resource("/", |r| r.method(Method::GET).f(index))
    }).bind("0.0.0.0:8088")
    .unwrap()
    .run();
}

fn stream_page((state, stream_key): (State<AppState>, actix_web::Path<String>)) -> impl Responder {
    let mut ctx = tera::Context::new();
    ctx.insert("stream_key", stream_key.as_str());

    HttpResponse::build(StatusCode::OK)
        .content_type("text/html; charset=utf-8")
        .body(state.template.render("video.html", &ctx).unwrap())
}

fn public_static(req: &HttpRequest<AppState>) -> Result<NamedFile> {
    let path: PathBuf = req.match_info().query("tail")?;
    Ok(NamedFile::open(Path::new("./public").join(path))?)
}

fn index(_req: &HttpRequest<AppState>) -> Result<NamedFile> {
    Ok(NamedFile::open(Path::new("./public").join("index.html"))?)
}
